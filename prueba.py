import yfinance as yf
#import datetime as dt
import pandas as pd
#import csv
data = pd.read_excel('config_alerts.xlsx')
print(data)
tickers= None
periodo=100
valores=yf.download(tickers,period=str(periodo)+"d")
print(valores)

sumaActualLenta=0
sumaAnteriorLenta=0
lenta=2
rapida=1

#aca sacamos la lenta
comienzaLenta=periodo-lenta-1
for i in range(comienzaLenta,periodo):
    if i==comienzaLenta:
        sumaAnteriorLenta += valores["Adj Close"][i]
    else:
        sumaActualLenta+=valores["Adj Close"][i]
        if i<periodo-1:
            sumaAnteriorLenta += valores["Adj Close"][i]
        else:
            continue
smaActualLenta=sumaActualLenta/lenta
smaAnteriorLenta=sumaAnteriorLenta/lenta

#aca sacamos la rapída
sumaActualRapida=0
sumaAnteriorRapida=0
comienzaRapida=periodo-rapida-1
for i in range(comienzaRapida,periodo):
    if i==comienzaRapida:
        sumaAnteriorRapida+=valores["Adj Close"][i]
    else:
        sumaActualRapida+=valores["Adj Close"][i]
        if i<periodo-1:
            sumaAnteriorRapida += valores["Adj Close"][i]
        else:
            continue
smaActualRapida=sumaActualRapida/rapida
smaAnteriorRapida=sumaAnteriorRapida/rapida

anterior=smaAnteriorRapida-smaAnteriorLenta
actual=smaActualRapida-smaActualLenta

#Aca te saca si corto para arriba o para abajo
if anterior < 0 < actual:
    print("ojo que corto para arriba")
if anterior > 0 > actual:
    print("ojo que corto para abajo")

#print(smaActualLenta)
#print(smaAnteriorLenta)
#print(smaActualRapida)
#print(smaAnteriorRapida)

#queda por hacer un csv que vamos a llenar a mano con los papeles, sma rapida y sma lenta
#modificar el codigo para que lea el csv y arme alguna variable donde esten los tickers con los periodos
#el periodo debe tomarlo como el mayor+1 de las sma en el csv
#investigar como enviar alertas