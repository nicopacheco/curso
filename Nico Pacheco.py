import yfinance as yf
import datetime as dt
import pandas as pd
from openpyxl import load_workbook
import os.path as path
import os
#hice a mano un archivo vacio en excel en el path siguiente
tickers=["AAPL","SPY","XLF"]
"""vacia=[""]
vacia1=pd.DataFrame(vacia)
vacia1.to_excel("baseDatosNP.xlsx",sheet_name="hoja1")"""


#path = r"C:\Users\Nicolas Pacheco\PycharmProjects\curso2\baseDatosNP.xlsx"
path = path.abspath(path.curdir)
#esto lo vi en internet
writer = pd.ExcelWriter(path, engine = 'openpyxl')
book = load_workbook(path)
writer.book = book

#Descargo la info de yfinance
periodo=10
valores=yf.download(tickers,period=str(periodo)+"d")
print(valores)

#Creo diccionario asi: {"Fecha":fecha,"Adj Close":fecha ] }
diccionario={}

fechas=[] #lista de fechas
for i in range(periodo):
    fecha=valores.index[i]
    fecha=fecha.strftime("%Y-%m-%d")
    fechas.append(fecha)
diccionario["Fechas"]=fechas

for papel in tickers: #lista de precios, itero cada ticker
    precios=[]
    for i in range(periodo): #itero por periodo
        precio=round(valores["Adj Close"][papel][i],2)
        precios.append(precio)
    diccionario["Precios"]=precios
    print(diccionario)
    break

    tabla=pd.DataFrame(diccionario) #la info que ya tengo la paso a DataFrame (pandas)
    tabla.to_excel(writer,sheet_name=papel) #paso el DataFrame a excel y le pongo el nombre de la hoja=ticker

writer.save()
writer.close()
