import yfinance as yf
import pandas as pd


data = pd.read_excel('config_alerts.xlsx', sheet_name='alerts') #levanto los datos del excel
alertas = []

'''Itera cada fila del archivo Excel y realiza los cálculos de medias'''
for fila in range(len(data)):
    ticker = data.loc[fila, 'ticker']
    mediaType = data.loc[fila, 'mediaType']
    periodo = int(data.loc[fila, 'periodo'])
    lenta = int(data.loc[fila, 'lenta'])
    rapida = int(data.loc[fila, 'rapida'])
    eMail = data.loc[fila, 'email']
    nombre = data.loc[fila, 'nombre']

    valores = yf.download(ticker, period=str(periodo) + "d")
    #print(valores.info())
    valores = valores.drop(['Open','High','Low','Close'],axis=1)
    #print(valores)

    if mediaType == 'SMA': #Media Movil Simple
        print(ticker)
        valores['media{}'.format(lenta)] = valores.loc[:,'Adj Close'].rolling(lenta).mean()
        valores['media{}'.format(rapida)] = valores.loc[:,'Adj Close'].rolling(rapida).mean()
        #print(valores)

    elif mediaType == 'EMA': #Media Movil Exponencial
        print(ticker)
        valores['media{}'.format(lenta)] = valores.loc[:,'Adj Close'].ewm(span=lenta, adjust=False).mean()
        valores['media{}'.format(rapida)] = valores.loc[:,'Adj Close'].ewm(span=rapida, adjust=False).mean()
        #print(valores)

    anterior = valores.iloc[-2]['media{}'.format(rapida)]-valores.iloc[-2]['media{}'.format(lenta)]
    actual = valores.iloc[-1]['media{}'.format(rapida)] - valores.iloc[-1]['media{}'.format(lenta)]
    #print(anterior)
    #print(actual)
    alerta = False
    alertaCruce = None
    if anterior < 0 and actual > 0:
        alerta = True
        alertaCruce = "Señal de Compra: {media}{fast} cortó hacia arriba {media}{low}".format(media=mediaType,\
                                                                                              fast=rapida, low=lenta)
        #print(alertaCruce)


    if anterior > 0 and actual < 0:
        alerta = True
        alertaCruce = "Señal de Venta: {media}{fast} cortó hacia abajo {media}{low}".format(media=mediaType,\
                                                                                              fast=rapida, low=lenta)
        #print(alertaCruce)

    volume = False
    avgVol = 0
    msgVol = ''
    if alerta == True:
        vol = list(valores['Volume'])
        avgVol = sum(vol)/len(vol)
        lastVol = vol[-1]
        if avgVol < lastVol:
            volume = True
            msgVol = ("{vol}".format(vol=volume))
            #print("VOLUMEN mayor al promedio: {vol}".format(vol=volume))
        else:
            msgVol = '-'

    tablaAlert=[]
    if alerta == True:
        tablaAlert.append(ticker)
        tablaAlert.append(alertaCruce)
        tablaAlert.append(msgVol)
        tablaAlert.append(eMail)
        tablaAlert.append(nombre)
        #print(tablaAlert)
        alertas.append(tablaAlert)

pd.options.display.max_columns = None


tabla = pd.DataFrame(alertas, columns = ['Ticker', 'Señal', 'Volumen', 'eMail', 'Nombre'])
print(tabla)
