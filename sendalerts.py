import smtplib, getpass
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from email.mime.application import MIMEApplication
from smtplib import SMTP
import sys


import logging

logging.basicConfig(level=logging.INFO,format='{asctime} {levelname} ({threadName:11s}) {message}', style='{')


from medias import tabla
correos=list(tabla["eMail"])
correos=list(dict.fromkeys(correos))


print(' Enviar e-mail con GMAIL')
user = "python.alertas@gmail.com"
password = "pymiercoles"
#password = getpass.getpass('Password: ')

# Para las cabeceras del email
#remitente = input('From, ejemplo: administrador <administrador@gmail.com')
# host y puerto SMTP de GMAIL
gmail = smtplib.SMTP('smtp.gmail.com', 587)
# protocolo de cifrado de datos utilizados por gmail
gmail.starttls()
# Credenciales
gmail.login(user, password)
logging.info("Abriendo correo")
# muestra la depuracion de la operacion de envio
gmail.set_debuglevel(1)

def enviarCorreo(destinatario,mensaje):
    logging.info(f"Por enviar corro a {destinatario}")
    asunto = "Alerta cruce medias python"
    header = MIMEMultipart()
    header['Subject'] = asunto
    header['From'] = user
    header['To'] = destinatario

    contenido = MIMEText(mensaje,"html") # CONTENT- type: text/html

    # texto normal. en vez de HTML, pongo PLAIN
    header.attach(contenido)
    gmail.sendmail(user,destinatario,header.as_string())


for correo in correos:
    filtro=tabla.copy()
    filtro=filtro.loc[filtro.eMail == correo]
    filtroNombre = list(filtro["Nombre"])
    filtroNombre = list(dict.fromkeys(filtroNombre))
    filtro=filtro.drop(["eMail","Nombre"],axis=1)

    html = """\
    <html>
      <head></head>
      <body>
      <p>Estimad@ <b>{0}</b>, éstas son las alertas del día:</p>
      <p></p>
        {1}
        
      </body>
    </html>
    """.format(filtroNombre[0], filtro.to_html())

    enviarCorreo(correo,html)

# Cerramos la conexion SMTP
gmail.quit()




