import yfinance as yf
import datetime as dt
import pandas as pd
import csv
import logging

logging.basicConfig(level=logging.INFO,format='{asctime} {levelname} ({threadName:11s}) {message}', style='{')

logging.info("Abriendo CSV")

data=csv.reader(open("tickers.csv"),delimiter=";") #abro y leo CSV
info=[fila for fila in data] #paso info del csv a una lista

for fila in range(1,len(info)): #itero cada fila del csv
    ticker=info[fila][0]
    periodo=int(info[fila][1])
    periodo+=1
    lenta = int(info[fila][2])
    rapida = int(info[fila][3])

    logging.info("Descargando info de {ticker}. Evaluando SMA Lenta de {smaLenta} ruedas y "
                 "SMA rapida de {smaRapida} ruedas".format(ticker=info[fila][0],smaLenta=lenta,smaRapida=rapida))
    valores=yf.download(ticker,period=str(periodo)+"d")

    sumaActualLenta = 0
    sumaAnteriorLenta = 0

    #logging.info("Calculando SMA Lenta")
    # aca sacamos la lenta
    comienzaLenta = periodo - lenta - 1
    for i in range(comienzaLenta, periodo):
        if i == comienzaLenta:
            sumaAnteriorLenta += valores["Adj Close"][i]
        else:
            sumaActualLenta += valores["Adj Close"][i]
            if i < periodo - 1:
                sumaAnteriorLenta += valores["Adj Close"][i]
            else:
                continue
    smaActualLenta = sumaActualLenta / lenta
    smaAnteriorLenta = sumaAnteriorLenta / lenta
print(smaActualLenta)
print(smaAnteriorLenta)
    logging.info("Calculando SMA Rapida")
    # aca sacamos la rapída
    sumaActualRapida = 0
    sumaAnteriorRapida = 0
    comienzaRapida = periodo - rapida - 1
    for i in range(comienzaRapida, periodo):
        if i == comienzaRapida:
            sumaAnteriorRapida += valores["Adj Close"][i]
        else:
            sumaActualRapida += valores["Adj Close"][i]
            if i < periodo - 1:
                sumaAnteriorRapida += valores["Adj Close"][i]
            else:
                continue
    smaActualRapida = sumaActualRapida / rapida
    smaAnteriorRapida = sumaAnteriorRapida / rapida

    anterior = smaAnteriorRapida - smaAnteriorLenta
    actual = smaActualRapida - smaActualLenta

    # Aca te saca si corto para arriba o para abajo
    alertaAlza=False
    alertaBaja = False
    if anterior < 0 and actual > 0:
        alertaAlza = True
        print("ALERTA: {papel} cortó para arriba".format(papel=ticker))
    if anterior > 0 and actual < 0:
        print("ALERTA: {papel} cortó para abajo".format(papel=ticker))
        alertaBaja = True

    #Verifica si el volumen es mayor o menor al promedio
    vol=False
    volPromedio=0
    if alertaAlza or alertaBaja:
        volPromedio= valores["Volume"].sum()/periodo
        ultimoVolumen=valores["Volume"][periodo-1]
        if volPromedio<ultimoVolumen:
            vol=True
            print("VOLUMEN mayor al promedio: {vol}".format(vol=vol))
